#!/usr/bin/python

# When you have an external screen, when moving the mouse between screens, 
# this gives focus to the last app used in the new screen
# It works by remembering the last app that had focus before the mouse left the screen.
#
# Tested with macOS 10.12.4
# run in a terminal with: python autofocus.py 
# sivann at gmail.com Spiros Ioannou


print('Please wait..')
import sys
import time
from Quartz.CoreGraphics import *
from AppKit import NSScreen
from AppKit import NSWorkspace
from Cocoa import NSRunningApplication, NSApplicationActivateIgnoringOtherApps

#leftresolution=1920
leftresolution =  NSScreen.mainScreen().frame().size.width
print('Detected left screen resolution: %d '%leftresolution)


#rightclick=(0,0)
#def mouseEvent(etype, posx, posy):
#	event = CGEventCreateMouseEvent(None, etype, (posx,posy), kCGMouseButtonLeft)
#	CGEventPost(kCGHIDEventTap, event)
#	
#def mouseMove(posx,posy):
#	mouseEvent(kCGEventMouseMoved, posx,posy)
#	
#def mouseWarp(posx,posy):
#	CGWarpMouseCursorPosition((posx, posy))
#	#mouseEvent(kCGEventMouseMoved, posx,posy)
#
#def mouseclick(posx,posy):
#	mouseEvent(kCGEventLeftMouseDown, posx,posy)
#	mouseEvent(kCGEventLeftMouseUp, posx,posy)
#
def mouseGetPos():
	ev = CGEventCreate(None)
	return CGEventGetLocation(ev)


lastfocus=''
lastpid=0
leftPid=0
rightPid=0
lastpos=mouseGetPos()
def focusPid(pid):
	if pid:
		app = NSRunningApplication.runningApplicationWithProcessIdentifier_(pid)
		app.activateWithOptions_(NSApplicationActivateIgnoringOtherApps)
		print 'Focusing pid:%d'%pid

while 1:
	currentpos = mouseGetPos()

	if lastfocus == '' and (currentpos.x > leftresolution):
		lastfocus='r'
		print 'Init lastfocus to ',lastfocus
	elif lastfocus == '':
		lastfocus='l'
		print 'Init lastfocus to ',lastfocus

	active_app = NSWorkspace.sharedWorkspace().activeApplication()

	#mouse moved to right screen
	if currentpos.x > leftresolution and lastfocus != 'r':
		leftPid = active_app['NSApplicationProcessIdentifier']
		print('Giving focus to right, lastfocus:%s, left pid:%d'%(lastfocus,leftPid))
		focusPid(rightPid)
		lastfocus='r'
	#mouse moved to left screen
	elif currentpos.x < leftresolution and lastfocus != 'l':
		print NSWorkspace.sharedWorkspace().frontmostApplication().localizedName()
		rightPid = active_app['NSApplicationProcessIdentifier']
		print('Giving focus to left, lastfocus:%s, right pid:%d'%(lastfocus,rightPid))
		focusPid(leftPid)
		lastfocus='l'

	#print('Pos (x,y)=(%d,%d), focus=%s'%(currentpos.x, currentpos.y, lastfocus))
	time.sleep(0.3)
	lastpos = currentpos
