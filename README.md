# screen focus follows mouse for MacOS #
When you have an external screen attached and this app is running, when moving the mouse between screens,
the last active application in the new screen gets the focus without clicking it. 

This works by remembering the last app that had focus before the mouse left each monitor.

* Tested with macOS 10.12.4
* run in a terminal with: python screenfocus.py


# BUGS: #
Gets confused if you switch to the same application PID on both screens, e.g. chrome developper tools and a chrome tab, 
but it works fine if the frontmost apps have different PID, e.g. a chrome tab and a terminal.


